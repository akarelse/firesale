<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(__ROOT__.'/libraries/BoxPacker/Box.php'); 

class Mybox implements DVDoug\BoxPacker\Box
{

    function __construct($description, $height, $width, $depth, $weight)
    {
        $this->description = $description;
        $this->width = $width;
        $this->height = $height;
        $this->depth =  $depth;
        $this->weight = $weight;


    }

 /**
     * Reference for box type (e.g. SKU or description)
     * @return string
     */
    public function getReference()
    {
    	return $this->description;
    }

    /**
     * Outer width in mm
     * @return int
     */
    public function getOuterWidth()
    {
    	return $this->width+1;
    }

    /**
     * Outer length in mm
     * @return int
     */
    public function getOuterLength()
    {
    	return $this->height+1;
    }

    /**
     * Outer depth in mm
     * @return int
     */
    public function getOuterDepth()
    {
    	return $this->depth+1;
    }

    /**
     * Empty weight in g
     * @return int
     */
    public function getEmptyWeight()
    {
    	return 10;
    }

    /**
     * Inner width in mm
     * @return int
     */
    public function getInnerWidth()
    {
    	return $this->width;
    }

    /**
     * Outer length in mm
     * @return int
     */
    public function getInnerLength()
    {
    	return $this->height;
    }

    /**
     * Outer depth in mm
     * @return int
     */
    public function getInnerDepth()
    {
    	return $this->depth;
    }

    /**
     * Total inner volume of packing in mm^3
     * @return int
     */
    public function getInnerVolume()
    {
    	return $this->width * $this->height * $this->depth;
    }

    /**
     * Max weight the packaging can hold in g
     * @return int
     */
    public function getMaxWeight()
    {
    	// return $this->weight;
        return 1000;
    }


}

