<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once(__ROOT__.'/libraries/BoxPacker/Item.php'); 

class Myitem implements DVDoug\BoxPacker\Item
{

   

    function __construct($description, $width, $height, $depth, $weight)
    {
        $this->description = $description;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
        $this->weight = $weight;
        $this->volume = $height * $depth * $width;

    }
 /**
     * Item SKU etc
     * @return string
     */
    public function getDescription()
    {
    	return $this->description;
    }

    /**
     * Item width in mm
     * @return int
     */
    public function getWidth()
    {
    	return $this->width;
    }

    /**
     * Item length in mm
     * @return int
     */
    public function getLength()
    {
    	return $this->height;
    }

    /**
     * Item depth in mm
     * @return int
     */
    public function getDepth()
    {
    	return $this->depth;
    }

    /**
     * Item weight in g
     * @return int
     */
    public function getWeight()
    {
    	return $this->weight;
    }
    
    /**
     * Item volume in mm^3
     * @return int
     */
    public function getVolume()
    {
    	return $this->volume;
    }

}