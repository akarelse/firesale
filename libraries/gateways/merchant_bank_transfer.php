<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * CI-Merchant Library
 *
 * Copyright (c) 2011-2012 Adrian Macneil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Merchant Mollie Class
 *
 * Payment processing using Mollie (iDEAL)
 * @link https://www.mollie.nl/support/documentatie/betaaldiensten/ideal/en/
 */
class Merchant_bank_transfer extends Merchant_driver
{
	public $useddata;

    public function __construct($driver = NULL)
    {
    	$this->useddata = array();
        parent::__construct();
        $this->_CI =& get_instance();
    }

	public function default_settings()
	{
		return array(
            'bank_account_nr' => ''
        );
	}

	public function get_order_data($arrayofstuff)
	{
		$this->useddata['order_id'] = $arrayofstuff['items'][0]['order_id'];
	}

	public function populate_form_data()
	{
		$output = array('bank_account_nr' => $this->setting('bank_account_nr'));
		return $output;
	}

	public function purchase()
	{	
		$request['amount'] = $this->amount_dollars();
		$request['currency'] = strtolower($this->param('currency'));

		$request['message'] = "Wij willen u vragen om het bedrag over te maken op rekening: " . $this->setting('bank_account_nr') . " ovv: order #" . $this->useddata['order_id'];
        return new Merchant_bank_transfer_response($request);
        
	}

}

class Merchant_bank_transfer_response extends Merchant_response
{
	

	public function __construct($response)
    {
		$this->_status = self::PENDING;
		$this->_message = $response['message'];
		$this->_amount = $response['amount'];
		$this->_currency = $response['currency'];
	}

	public function amount()
	{
		return $this->_amount;
	}

}

